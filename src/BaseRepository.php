<?php

namespace NextEngine;
//require_once 'nextEngineApiClientSingleton.php';

abstract class BaseRepository{
	//private $_client;
	
	protected function __construct($clientId,$clientSecret ,$userName, $passWord){
		$_client = API\NextEngineApiClient::getInstance($clientId,$clientSecret ,$userName, $passWord); 
		//var_dump($_client);
		return $_client;
	}

	/*
	protected function getClient(){
		return $_client;
	}
	*/
}
