<?php

namespace NextEngine\Models;
use \NextEngine\BaseRepository;

class ProductStockRepository extends BaseRepository{
		private	  $searchStock        = '/api_v1_master_stock/search';
		private   $searchStockHistory = '/api_v1_master_stockiohistory/search';
		private	  $uploadURL 		  = '/api_v1_master_stock/search';
		protected $client;
		private static $instance;
			
		protected function __construct($clientId,$clientSecret ,$userName, $passWord){	
			$this->client = parent::__construct($clientId,$clientSecret ,$userName, $passWord);
			$this->client->init();
		}

		static function createInstance($clientId,$clientSecret ,$userName, $passWord){
			if(!self::$instance){
				$instance = new ProductStockRepository($clientId,$clientSecret ,$userName, $passWord);
			}
			return $instance;
		}
		
		function getProductStockById($id){
			$apiParams = array(); 
			$apiParams['fields'] = 'stock_goods_id, stock_quantity, stock_free_quantity,stock_advance_order_quantity,stock_advance_order_free_quantity';
			$apiParams['stock_goods_id-like']= $id."%";
			$content = $this->client->execute($this->searchStock,$apiParams);
			$data = $content->data;
			//$data = $data[0];
			return new ProductStock($data);
		}
		
		function getProductStockBySKU($sku){
			$apiParams = array(); 
			$apiParams['fields'] = 'stock_goods_id, stock_quantity, stock_free_quantity,stock_advance_order_quantity,stock_advance_order_free_quantity';
			$apiParams['stock_goods_id-eq']= $sku;
			$content = $this->client->execute($this->searchStock,$apiParams);
			$data = $content->data;
			$data = $data[0];
			return new ProductStock($data);
		}

		function getCurrentProductStockAll(){	
			$apiParams = array(); 
			$apiParams['fields'] = 'stock_goods_id, stock_quantity, stock_free_quantity,stock_advance_order_quantity,stock_advance_order_free_quantity';
			$apiParams['stock_quantity-gt']= 0;
			$content = $this->client->execute($this->searchStock,$apiParams);
			$data = $content->data;
			$data = $data[0];
			return new ProductStock($data);
		}

		function getPreOrderProductStockAll(){	
			$apiParams = array(); 
			$apiParams['fields'] = 'stock_goods_id, stock_quantity, stock_free_quantity,stock_advance_order_quantity,stock_advance_order_free_quantity';
			$apiParams['stock_advance_order_quantity-gt']= 0;
			$apiParams['limit']=10;
			$content = $this->client->execute($this->searchStock,$apiParams);
			$data = $content->data;
			$data = $data[0];
			return new ProductStock($data);
		}
	   
		function getProductStockHistoryBySKU($sku){
			$apiParams = array(); 
			$apiParams['fields'] = 'stock_io_history_id, stock_io_history_shop_id, stock_io_history_goods_id, stock_io_history_date,stock_io_history_before_stock_quantity, stock_io_history_after_stock_quantity, stock_io_history_quantity, stock_io_history_cut_form_id, stock_io_history_io_flag, stock_io_history_reason, stock_io_history_pic_name';
			
			$apiParams['stock_io_history_goods_id-eq'] = $sku;
			$apiParams['limit']=10;
			$content = $this->client->execute($this->searchStockHistory,$apiParams);
			$data = $content->data;
			$data = $data[0];
			return new ProductStock($data);
		}	

		function getProductStockHistoryByDate($yyyymmdd){
			$apiParams = array(); 
			$apiParams['fields'] = 'stock_io_history_id, stock_io_history_shop_id, stock_io_history_goods_id, stock_io_history_date,stock_io_history_before_stock_quantity, stock_io_history_after_stock_quantity, stock_io_history_quantity, stock_io_history_cut_form_id, stock_io_history_io_flag, stock_io_history_reason, stock_io_history_pic_name';
			
			$apiParams['stock_io_history_date-eq'] = $yyyymmdd;
			$apiParams['limit']=10;
			$content = $this->client->execute($this->searchStockHistory,$apiParams);
			$data = $content->data;
			$data = $data[0];
			return new ProductStock($data);
		}

//		function update(array $models) {
//				foreach() {
//					if(!$model instanceof ProductMeta) throw new Exception()
//						$model
//				{}
//		}
		//$headers is array ,$values($sku => $somthing)
			
	
		function formatForCSV($headers,$values){
		    $header = NULL;
		    foreach($headers as $value){
				if($value === end($headers)){
					$header = $header.'"'.$value.'"'."\n";
				}else{
					$header = $header.'"'.$value.'",';
				}
			}

			$data = NULL;
			$i = 0;
			$last = count($values);
			foreach($values as $datas){
				$i++;	
				if($i === $last){
					$data = $data.'"'.$value.'"'."\n";
				}else{
					$data = $data.'"'.$value.'",';
				}
			}	
	
			$data = $header.$data;
			$data = mb_convert_encoding($data,"SJIS", "auto");   
			return $data;
		}
	
		function uploadProductStockCSV($data){
			$apiParams = array();
			$apiParams['data_type'] ='csv';
			$apiParams['data'] = $data;   
			$content = $this->client->execute($this->uploadURL,$apiParams);   
			return $content;
		}
		
		function updateQuantity(ProductStock $model) {
			$csv     = $model->QuantitiyToCsv();
			$headers = $csv['headers'];
			$data    = $csv['data'];
			$CSVdata = $this->formatForCSV($headers,$data);
			$content = $this->uploadProductStockCSV($CSVdata);
			return $content;
		}

		function updatePreOrderQuantity(ProductStock $model) {
			$csv     = $model->PreOrderQuantitiyToCsv();
			$headers = $csv['headers'];
			$data    = $csv['data'];
			$CSVdata = $this->formatForCSV($headers,$data);
			$content = $this->uploadProductStockCSV($CSVdata);
			return $content;
		}
}
 
//echo $this->accessToken;
