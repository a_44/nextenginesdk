<?php

namespace NextEngine\Models;

class ProductStock {
  /*	
   * @param array
   */
	private	  $uploadURL = '/api_v1_master_stock/search';
	protected $fields;

  	public function __construct($fields) {
    	$this->fields = $fields;
  	}

	public function setQuantity($num) {
		if(!is_numeric($num)) throw new \IllegalArgumentException('Num has to be number.');
    	$this->fields->zaiko_su = $num;
	}

	public function setPreOrderQuantity($num){
		if(!is_numeric($num)) throw new \IllegalArgumentException('Num has to be number.');
		$this->fields->yoyaku_zaiko_su = $num;
	}
	
	public function QuantityToCSV(){
		if(!$this->fields) throw new \IllegalArgumentException('At first create stockInstance');
		$headers = array('syohin_code','zaiko_su');
		$data    = array($this->fields->stock_goods_id,$this->fields->zaiko_su);	
		$array['headers'] = $headers;
	    $array['data']    = $data;	
		return  $array;	
	}

	public function PreOrderQuantityToCSV(){
		if(!$this->fields) throw new \IllegalArgumentException('At first create stockInstance');
		$headers = array('syohin_code','yoyaku_zaiko_su');
		$data    = array($this->fields->stock_goods_id,$this->fields->yoyaku_zaiko_su);	
		$array['headers'] = $headers;
	    $array['data']    = $data;	
		return  $array;	
	}

}
