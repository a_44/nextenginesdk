<?php

namespace NextEngine\Models;

use \NextEngine\BaseRepository;
//require_once 'nextEngineApiClientSingleton.php';
//require_once 'baseRepository.php';
//require_once 'productMeta.php';
//require_once 'productStock.php';

class ProductMetaRepository extends BaseRepository{
		private   $searchProduct      = '/api_v1_master_goods/search';
		private   $searchStockHistory = '/api_v1_master_stockiohistory/search';
		private   $uploadProduct      = '/api_v1_master_goods/upload';
		
		protected $client;
		private static $instance;
			
		protected function __construct($clientId,$clientSecret ,$userName, $passWord){	
			$this->client = parent::__construct($clientId,$clientSecret ,$userName, $passWord);
			$this->client->init();
		}

		static function createInstance($clientId,$clientSecret ,$userName, $passWord){
			if(!self::$instance){
				$instance = new ProductMetaRepository($clientId,$clientSecret ,$userName, $passWord);
			}
			return $instance;
		}
		
		function getProductMetaById($id){
			$apiParams = array();	
			$apiParams['fields'] = 'goods_id, goods_name, goods_type_id, goods_cost_price, goods_selling_price, goods_display_price, goods_supplier_id, supplier_name' ;
			$apiParams['goods_representation_id-eq'] = $id;
			$content = $this->client->execute($this->searchProduct,$apiParams);
			$data = $content->data;
			//var_dump($data);
			return new ProductMeta($data);
		}

		function getProductMetaBySKU($sku){
			$apiParams = array();	
			$apiParams['fields'] = 'goods_id, goods_name, goods_type_id, goods_cost_price, goods_selling_price, goods_display_price, goods_supplier_id, supplier_name, goods_representation_id' ;
			$apiParams['goods_id-eq'] = $sku;
			$content = $this->client->execute($this->searchProduct,$apiParams);
			$data = $content->data;
			$data = $data[0];
			return new ProductMeta($data);
		}

		function formatForCSV($headers,$values){
		    $header = NULL;
		    foreach($headers as $value){
				if($value === end($headers)){
					$header = $header.'"'.$value.'"'."\n";
				}else{
					$header = $header.'"'.$value.'",';
				}
			}

			$data = NULL;
			$i = 0;
			$last = count($values);
			foreach($values as $datas){
				$i++;	
				if($i === $last){
					$data = $data.'"'.$value.'"'."\n";
				}else{
					$data = $data.'"'.$value.'",';
				}
			}	
	
			$CSVdata = $header.$data;
			$CSVdata = mb_convert_encoding($CSVdata,"SJIS", "auto");   
			return $CSVdata;
		}
	
		function uploadProductCSV($CSVdata){
			$apiParams = array();
			$apiParams['data_type'] ='csv';
			$apiParams['data'] = $CSVdata;   
			$content = $this->client->execute($this->uploadProduct,$apiParams);   
			return $content;
		}
		
//		function update(array $models) {
//				foreach() {
//					if(!$model instanceof ProductMeta) throw new Exception()
//						$model
//				{}
//		}
		
		function update(ProductMeta $model) {
			$csv     = $model->toCsv();
			$headers = $csv['headers'];
			$data    = $csv['data'];
			//var_dump($data);
			$CSVdata = $this->formatForCSV($headers,$data);
			$content = $this->uploadProductCSV($CSVdata);
			return $content;
		}
}
 
//echo $this->accessToken;
