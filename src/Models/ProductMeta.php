<?php

namespace NextEngine\Models;

class ProductMeta {
  /*	
   * @param array
  */	
	protected $fields;
	// setGoodsNameを使いたいんだけど、$this->setGoodsNmaeやself::setGoodsNameは使えないですか？	
	public static function createNewProduct($sku, $name, $typeId, $price, $cost, $supplierId, $id) {
		$fields = new \stdClass();
		$fields->goods_id                = $sku;
		$fields->goods_name              = $name;
		$fields->goods_type_id           = $typeId;
		$fields->goods_selling_price     = $price;
		$fields->goods_cost_price        = $cost;
		$fields->goods_supplier_id       = $supplierId;
		$fields->goods_representation_id = $id;		
		return new ProductMeta($fields);
	}

  	public function __construct($fields) {
    	$this->fields = $fields;
  	}

	public function setGoodsSKU($sku){
		if(!preg_match("/^[a-zA-Z0-9-]+$/", $sku)) throw new Exception('sku has to be a-z,0-9,&');
		$this->fields->goods_id = $sku;
	}

	
	public function setGoodsName($name) {
		if(!is_string($name)) throw new Exception('Name has to be string.');
		if(strlen($name) > 255) throw new Exception('Name has to be less than 255 chars.');

    	$this->fields->goods_name = $name;
	}


	public function setGoodsTypeId($typeId){
		if($typeId != 0 and $typeId != 10 and $typeId != 20 ) throw new Exception('typeId has to be 0 or 10 or 20');
		$this->fields->goods_type_id = $typeId;
	}
	
	
	public function setSellingPrice($price){
		if(!is_numeric($price)) throw new Exception('price has to be number');
		$this->fields->goods_selling_price = $price;
	}

	
	public function setCostPrice($cost){
		if(!is_numeric($cost)) throw new Exception('cost has to be number');
		$this->fields->goods_cost_price = $cost;
	}


	public function setSupplierId($supplierId){
		if(strlen($supplierId) > 4) throw new Exception('supplierId has to less than 4 char');
		$this->fields->goods_supplier_id = $supplierId;
	}

	public function setProductId($id){
		if(strlen($id)> 30 ) throw new Exception('daihyoSyohinCode has to less than 30 char'); 
		$this->fields->goods_representation_id = $id;
	}
	
	public function toCSV(){
		if(!$this->fields)throw new Exception('need to cleate productMeta instance');
		$headers = array("syohin_code","syohin_name","syohin_kbn","genka_tnk","baika_tnk","sire_code","daihyo_syohin_code");
		$data    = array($this->fields->goods_id,$this->fields->goods_name,$this->fields->goods_type_id,round($this->fields->goods_cost_price),round($this->fields->goods_selling_price),$this->fields->goods_supplier_id, $this->fields->goods_representation_id);
		$response['headers']    = $headers;
		$response['data']       = $data;
		return $response;
	}
	
}

