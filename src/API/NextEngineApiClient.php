<?php
	namespace NextEngine\API;

    class NextEngineApiClient {	

		private static $instance;	
		private $accessToken;
		private $refreshToken;
		private $clientId;
		private $clientSecret;
		private $username;
		private $password;
		private $guzzleClient;
		protected $client;

	private function __construct($clientId, $clientSecret, $username, $password ){
		$this->clientId      = $clientId;
		$this->clientSecret  = $clientSecret;
		$this->username      = $username;
		$this->password      = $password;
    }
	
	static function getInstance($clientId, $clientSecret, $username, $password){
		if(!self::$instance){
			$instance = new NextEngineApiClient($clientId, $clientSecret, $username, $password);
			echo "createInstance\n";
			//var_dump($instance);
		}
		//var_dump($instance);
		return $instance;
		
	}

	function init() {
		if($this->client) {
			throw new Exception("Already inited");
		}
		$this->login($this->username, $this->password);
		$ret = $this->doUidAndState($this->clientId, $this->clientSecret);
		$tokens = $this->initTokens($ret['uid'], $ret['state']);
		$this->accessToken  = $tokens->access_token;
		$this->refreshToken = $tokens->refresh_token;
	}

	function getAccessToken() {
		if(!$this->accessToken){
			throw new Exception("Client is not inited");
		}
		return $this->accessToken;
	}

	function getRefreshToken() {
		if(!$this->refreshToken){
			throw new Exception("Client is not inited");
		}
        return $this->refreshToken;
	 }

    
    /*
    function init($username, $password) {
 		if(!$this->accessToken || !$this->refreshToken) {
			$res  	  	  = $this->login($username, $password);
			$tokens = $this->initTokens($res['uid'], $res['state']);
			$this->accessToken = $tokens['access_token'];
		}
  	}
	*/

  	//To Get uid and state,login NextEngine with username & password
	private function login($username, $password) {
		//login NextEngine
		$this->client = new \Goutte\Client();
		$crawler = $this->client->request('GET', 'https://base.next-engine.org/users/sign_in');
		//$page_title = $crawler->filter('head > title')->text();

		$login_form = $crawler->filter('form')->form();
		$login_form['user[login_code]'] = $username; //'tsutsumi-kouji';
		$login_form['user[password]']   = $password; //'nsn-030619';
		$this->client->submit($login_form);
		echo "success login\n";
	}

	private function doUidAndState($clientId, $clientSecret) {	
		//And then,send client_id and client_secret
		$url = 'https://base.next-engine.org/users/sign_in/?client_id='.$clientId.'&client_secret='.$clientSecret;
		$this->client->followRedirects(false);
		$this->client->request('GET',$url);

		$redirectHeaders = $this->client->getInternalResponse()->getHeaders();
		$getArray        = parse_url($redirectHeaders['Location'][0]);
		$queries = array();
		foreach (explode('&',$getArray['query']) as $v) {
			$kv = explode('=', $v);
			$queries[$kv[0]] = $kv[1];
		}

		return $queries;
	}

  //Get accesstoken and refreshtoken with guzzle which is httpclient like a curl 
  private function initTokens($uid, $state) {
	
	if(!$uid || !$state){
		$this->login($username,$password);
	}
	
	$this->guzzleClient = new \GuzzleHttp\Client();
	$file_data    = "uid=".$uid."&state=".$state;
	$postRes = $this->guzzleClient->post('https://api.next-engine.org/api_neauth/',[
			'form_params' =>[
				'uid'       => $uid,
			 	'state'     => $state,
			    'file_data' => $file_data
			], 
	]);
	
	$postRes = json_decode($postRes->getBody()->getContents());

	return $postRes;
  }

	function execute($path, $apiParams) {
		if(!$this->accessToken || !$this->refreshToken){
			throw new Exception("token is nothing,plz new samleClint() and init()"); 
		}	
		$apiParams['access_token']  = $this->accessToken;	
		$apiParams['refresh_token'] = $this->refreshToken;
		$formParams['form_params']  = $apiParams; 

		//$this->guzzleClient = new GuzzleHttp\Client();	
		$ret = $this->guzzleClient->post('https://api.next-engine.org'.$path,$formParams);		
		$content = json_decode($ret->getBody()->getContents());
		//$content = $ret->getBody()->getContents();
		//$content->json();		
		if($content->access_token) {
			$this->accessToken = $content->access_token;
		}
		if($content->refresh_token) {
			$this->refreshToken = $content->refresh_token;
		}
		return $content;
	}
}

/*
$client = NextEngineApiClient::getInstance('GoM4xPvSyk6URX', '3bZ5rWCtE4QohaFYjqMmzP9iKwpyDROe7u1cUSLl','tsutsumi-kouji', 'nsn-030619');
//var_dump($client);
$client->init();
echo "access_token=" .$client->getAccessToken()."\n";
echo "refresh_token=".$client->getRefreshToken()."\n";
*/


